<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class userViewTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testViewUser()
    {
        $response = $this->get('/user');

        $response->assertSeeText('Tambah Pegawai');
    }
}
