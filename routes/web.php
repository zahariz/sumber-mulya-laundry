<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Routing analytical dashboard
Route::get("/dashboard", [\App\Http\Controllers\HomeController::class, 'home'])->name('home');

//Routing data master paket
Route::controller(\App\Http\Controllers\PaketController::class)
    ->middleware(\App\Http\Middleware\OnlyMemberMiddleware::class)->group(function (){
        Route::get('/paket', 'showPaket')->name('paket.list');
        Route::get('/paket/create', 'createPaket')->name('paket.create');
        Route::post('/paket/create', 'savePaket');
        Route::get('/paket/edit/{id}', 'getPaket')->name('paket.edit');
        Route::put('/paket/edit/{id}', 'savePaket')->name('paket.update');
        Route::get('/paket/delete/{id}', 'deletePaket')->name('paket.delete');
});

//Routing data master barang
Route::controller(\App\Http\Controllers\BarangController::class)
    ->middleware(\App\Http\Middleware\OnlyMemberMiddleware::class)->group(function (){
        Route::get('/barang', 'showBarang')->name('barang.list');
        Route::get('/barang/create', 'createBarang')->name('barang.create');
        Route::post('/barang/create', 'saveBarang');
        Route::get('/barang/edit/{id}', 'getBarang')->name('barang.edit');
        Route::put('/barang/edit/{id}', 'saveBarang')->name('barang.update');
        Route::get('/barang/delete/{id}', 'deleteBarang')->name('barang.delete');
});

//Routing data master pegawai
Route::controller(\App\Http\Controllers\PegawaiController::class)
    ->middleware(\App\Http\Middleware\OnlyMemberMiddleware::class)
    ->group(function (){
        Route::get('/pegawai', 'showPegawai')->name('pegawai.list');
        Route::get('/pegawai/create', 'createPegawai')->name('pegawai.create');
        Route::post('/pegawai/create', 'savePegawai');
        Route::get('/pegawai/edit/{id}', 'getPegawai')->name('pegawai.edit');
        Route::get('/pegawai/detail/{id}', 'getDetailPegawai')->name('pegawai.detail');
        Route::put('/pegawai/edit/{id}', 'savePegawai')->name('pegawai.update');
        Route::get('/pegawai/delete/{id}', 'deletePegawai')->name('pegawai.delete');
    });



//Routing data transaksi pelanggan

Route::controller(\App\Http\Controllers\PelangganController::class)
    ->middleware(\App\Http\Middleware\OnlyMemberMiddleware::class)
    ->group(function (){
        Route::get('/pelanggan', 'showPelanggan')->name('pelanggan.list');
        Route::get('/pelanggan/create', 'createPelanggan')->name('pelanggan.create');
        Route::post('/pelanggan/create', 'createOrUpdate');
        Route::get('/pelanggan/edit/{id}', 'getPelangganById')->name('pelanggan.edit');
        Route::put('/pelanggan/edit/{id}', 'createOrUpdate')->name('pelanggan.update');
        Route::get('/pelanggan/delete/{id}', 'deletePelanggan')->name('pelanggan.delete');
    });


//Routing data Transaksi
Route::controller(\App\Http\Controllers\TransaksiController::class)
    ->middleware(\App\Http\Middleware\OnlyMemberMiddleware::class)
    ->group(function (){
        Route::get('/transaksi', 'showTransaksi')->name('transaksi.create');
        Route::get('/transaksipelanggan/{id}', 'ajaxTransaksiPelanggan')->name('ajax.pelanggan');
        Route::get('/transaksipaket/{id}', 'ajaxTransaksiPaket')->name('ajax.paket');
        Route::post('/transaksicart', 'addCartBarang')->name('ajax.cartBarang');
        Route::get('/transaksideletecart/{id}', 'removeCartBarang')->name('ajax.removeCartBarang');
        Route::post('/transaksi', 'saveTransaksi');
    });

//Route::get('/transaksi', function (){
//    return view('dashboard/transaksi/index', [
//        'title' => 'Transaksi',
//        'pelanggan' => [
//            'id' => 1,
//            'nama' => 'Riki Kurniawan',
//            'kontak' => '0225417444'
//        ]
//    ]);
//});

//
////Routing data master user
//Route::get('/pegawai', function (){
//    return view('dashboard/pegawai/index', [
//        'title' => 'Pegawai'
//    ]);
//});
//Route::get('/user/create', function (){
//    return view('dashboard/pegawai/add', [
//        'title' => 'Pegawai'
//    ]);
//});
//Route::get('/user/edit', function (){
//    return view('dashboard/pegawai/edit', [
//        'title' => 'Pegawai'
//    ]);
//});


Route::controller(\App\Http\Controllers\UserController::class)
    ->middleware(\App\Http\Middleware\OnlyMemberMiddleware::class)
    ->group(function (){
    Route::get('/user', 'showUser')->name('user.list');
    Route::get('/user/create', 'createUser')->name('user.create');
    Route::post('/user/create', 'saveUser');
    Route::get('/user/edit/{id}', 'getUser')->name('user.edit');
    Route::put('/user/edit/{id}', 'saveUser')->name('user.update');
    Route::get('/user/delete/{id}', 'deleteUser')->name('user.delete');
});


//Routing auth
Route::controller(\App\Http\Controllers\LoginController::class)->group(function (){
    Route::get('/login', 'login')->middleware(\App\Http\Middleware\OnlyGuestMiddleware::class);
    Route::post('/login', 'doLogin')->middleware(\App\Http\Middleware\OnlyGuestMiddleware::class);
    Route::post('logout', 'doLogout')->middleware(\App\Http\Middleware\OnlyMemberMiddleware::class);
});

