<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksis', function (Blueprint $table) {
            $table->id();
            $table->string('kdTransaksi')->index()->nullable();
            $table->bigInteger('idPaket')->unsigned()->index()->nullable();
            $table->bigInteger('idBarang')->unsigned()->index()->nullable();
            $table->integer('beratPaket');
            $table->integer('waktu');
            $table->integer('total');
            $table->foreign('kdTransaksi')->references('kdTransaksi')->on('transaksis');
            $table->foreign('idPaket')->references('id')->on('pakets');
            $table->foreign('idBarang')->references('id')->on('barangs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksis');
    }
};
