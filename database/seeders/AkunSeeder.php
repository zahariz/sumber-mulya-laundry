<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
          [
              'user' => 'DINA',
              'password' => Hash::make(123456),
              'role' => 'SuperAdmin'
          ],
            [
                'user' => 'RIKI',
                'password' => Hash::make(123456),
                'role' => 'Admin'
            ]
        ];

        foreach ($user as $key => $value){
            User::create($value);
        }
    }
}
