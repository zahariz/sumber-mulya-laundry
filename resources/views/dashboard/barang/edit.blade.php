@extends('dashboard.layouts.template')

@section('container')

    <div class="card">
        <div class="card-header">
            <h5>{{ isset($barang) ? "Edit Paket" : "Tambah Paket" }}</h5>
        </div>
        <form class="form theme-form needs-validation" novalidate="" role="form"  action="{{ isset($barang) ? route('barang.update',$barang->id) : route('barang.create') }}" method="POST">
            @csrf
            @isset($barang)
                @method('PUT')
            @endisset
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="kdBarang">Kode Barang</label>
                            <input class="form-control btn-pill" type="text" name="kdBarang" placeholder="KP-001" value="{{$barang->kdBarang ?? ''}}" required="">
                            <div class="invalid-feedback">Kode paket tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="namaBarang">Nama Barang</label>
                            <input class="form-control btn-pill"  type="text" placeholder="Celana" name="namaBarang" value="{{$barang->namaBarang ?? ''}}" required="">
                            <div class="invalid-feedback">Kode paket tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
                <a href="/barang" class="btn btn-light" >Cancel</a>
            </div>
        </form>
    </div>

@endsection
