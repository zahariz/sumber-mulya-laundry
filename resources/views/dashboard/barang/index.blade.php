@extends('dashboard.layouts.template')

@section('container')
    <!-- Base styles-->
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>List Barang Laundry </h5>
                    <a href="{{route('barang.create')}}" class="btn btn-primary btn-sm float-right" type="button" data-original-title="Tambahkan List Barang" title="">Tambah Barang</a>
                </div>
                <div class="card-body">
                    @if (session('msg'))
                        <div class="text-left alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="bell"></i>
                            <p>{{session('msg')}}</p>
                            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="display" id="example-style-1">
                            <thead>
                            <tr>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($barang as $row)
                                <tr>
                                <td>{{$row->kdBarang}}</td>
                                <td>{{$row->namaBarang}}</td>
                                    <td>
                                        <a href="{{route('barang.edit', $row->id)}}" class="btn btn-success btn-xs float-left mr-2" type="submit" data-original-title="Edit barang ini" title="">Edit</a>
                                        <a href="{{route('barang.delete', $row->id)}}" class="btn btn-danger btn-xs float-left" type="submit" data-original-title="Hapus barang ini" >Hapus</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Base styles Ends-->
@endsection
