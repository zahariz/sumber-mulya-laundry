@extends('dashboard.layouts.template')

@section('container')

    <div class="card">
        <div class="card-header">
            <h5>Tambah {{ $title  }}</h5>
        </div>
        <form class="form theme-form needs-validation" action="/barang/create" method="POST" novalidate="">
            @csrf
{{--            @if(isset($error))--}}
{{--                <div class="text-left alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="bell"></i>--}}
{{--                    <p>{{$error}}</p>--}}
{{--                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>--}}
{{--                </div>--}}
{{--            @endif--}}
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleFormControlInput5">Kode Barang</label>
                            <input class="form-control btn-pill" id="exampleFormControlInput5" placeholder="KBAR01" type="text" name="kdBarang"  required="">
                            <div class="invalid-feedback">Kode Barang ga boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="validationCustomUsername">Nama Barang</label>
                            <input class="form-control btn-pill" id="validationCustomUsername" type="text" placeholder="Celana" name="namaBarang" required="">
                            <div class="invalid-feedback">Nama Barang ga boleh kosong</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
                <input class="btn btn-light" type="reset" value="Cancel">
            </div>
        </form>
    </div>

@endsection
