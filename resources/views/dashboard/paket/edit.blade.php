@extends('dashboard.layouts.template')

@section('container')

    <div class="card">
        <div class="card-header">
            <h5>{{ isset($paket) ? "Edit Paket" : "Tambah Paket" }}</h5>
        </div>
        <form class="form theme-form needs-validation" novalidate="" role="form" method="POST"
              action="{{ isset($paket) ? route('paket.update',$paket->id) : route('paket.create') }}">
            @csrf
            @isset($paket)
                @method('PUT')
            @endisset
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleFormControlInput5">Kode Paket</label>
                            <input class="form-control btn-pill" id="exampleFormControlInput5" type="text" name="kdPaket" placeholder="KP-001" value="{{ $paket->kdPaket ?? '' }}" required="">
                            <div class="invalid-feedback">Kode paket tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Nama Paket</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="text" placeholder="Laundry Express/Kg" name="namaPaket" value="{{ $paket->namaPaket ?? '' }}" required="">
                            <div class="invalid-feedback">Nama paket tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Harga</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="number" placeholder="15000" name="harga" value="{{ $paket->harga ?? '' }}" required="">
                            <div class="invalid-feedback">Harga paket tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Waktu Paket</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="number" placeholder="3" name="waktuPaket" value="{{ isset($paket->waktuPaket) ? $paket->waktuPaket/86400 : ''}}" required="">
                            <div class="invalid-feedback">Waktu paket tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
                <a href="/paket" class="btn btn-light" >Cancel</a>
            </div>
        </form>
    </div>

@endsection
