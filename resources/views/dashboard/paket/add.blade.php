@extends('dashboard.layouts.template')

@section('container')

    <div class="card">
        <div class="card-header">
            <h5>Tambah {{ $title  }}</h5>
        </div>
        <form class="form theme-form">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleFormControlInput5">Kode Paket</label>
                            <input class="form-control btn-pill" id="exampleFormControlInput5" type="text" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Nama Paket</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="text" placeholder="Laundry Express/Kg">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Harga</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="number" placeholder="15000">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
                <input class="btn btn-light" type="reset" value="Cancel">
            </div>
        </form>
    </div>

@endsection
