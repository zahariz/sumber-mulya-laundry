@extends('dashboard.layouts.template')

@section('container')
<!-- Base styles-->
<div class="row second-chart-list third-news-update">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Paket Laundry </h5>
            <a href="{{route('paket.create')}}" class="btn btn-primary btn-sm float-right" type="button" data-original-title="Tambahkan List Paket Laundry" title="">Tambah Paket</a>
        </div>
        <div class="card-body">
            @if (session('msg'))
                <div class="text-left alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="bell"></i>
                    <p>{{session('msg')}}</p>
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
            @endif
            <div class="table-responsive">
                <table class="display" id="example-style-1">
                    <thead>
                    <tr>
                        <th>Kode Paket</th>
                        <th>Nama Paket</th>
                        <th>Harga</th>
                        <th>Waktu Paket</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paket as $row)
                        <tr>
                        <td>{{$row->kdPaket}}</td>
                        <td>{{$row->namaPaket}}</td>
                        <td>{{$row->harga}}</td>
                        <td>{{$row->waktuPaket / 86400}} Hari</td>
                        <td>
                            <a href="{{route('paket.edit', $row->id)}}" class="btn btn-success btn-xs" type="button" data-original-title="Edit paket ini" title="">Edit</a>
                            <a href="{{route('paket.delete', $row->id)}}" class="btn btn-danger btn-xs" type="button" data-original-title="Hapus paket ini" title="">Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Kode Paket</th>
                        <th>Nama Paket</th>
                        <th>Harga</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Base styles Ends-->
@endsection
