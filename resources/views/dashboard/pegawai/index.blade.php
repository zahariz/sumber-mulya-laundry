@extends('dashboard.layouts.template')

@section('container')
    <!-- Base styles-->
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Pegawai </h5>
                    <a href="{{route('pegawai.create')}}" class="btn btn-primary btn-sm float-right" type="button" data-original-title="Tambahkan List Pegawai" title="">Tambah Pegawai</a>
                </div>
                <div class="card-body">
                    @if (session('msg'))
                        <div class="text-left alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="bell"></i>
                            <p>{{session('msg')}}</p>
                            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="display" id="example-style-1">
                            <thead>
                            <tr>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Gender</th>
                                <th>Jabatan</th>
                                <th>Masa Kerja</th>
                                <th>Gaji</th>
                                <th>Alamat</th>
                                <th>No. Telp</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pegawai as $row)
                            <tr>
                                @php($difference = time() - $row->mulaiKerja)
                                @php($data['years'] = abs(floor($difference / 31536000)))
                                @php($data['days'] = abs(floor(($difference - ($data['years'] * 31536000)) / 86400)))
                                @php($data['hours'] = abs(floor(($difference - ($data['years'] * 31536000) - ($data['days'] * 86400)) / 3600)))
                                @php($data['minutes'] = abs(floor(($difference - ($data['years'] * 31536000) - ($data['days'] * 86400) - ($data['hours'] * 3600)) / 60)))
                                @php($timeString = '')
                                @if($data['years'] > 0)
                                    @php($timeString .= $data['years'] . " Years, ")
                                @endif
                                @if($data['days'] > 0)
                                    @php($timeString .= $data['days'] . " Days, ")
                                @endif
                                @if($data['hours'] > 0)
                                    @php($timeString .= $data['hours'] . " Hours, ")
                                @endif
                                @if($data['minutes'] > 0)
                                    @php($timeString .= $data['minutes'] . " Minutes")
                                @endif
                                <td>{{$row->nik}}</td>
                                <td>{{$row->namaPegawai}}</td>
                                <td>{{$row->gender}}</td>
                                <td>{{$row->jabatan}}</td>
                                <td>{{ $timeString}}</td>
                                <td>{{$row->gaji}}</td>
                                <td>{{$row->alamat}}</td>
                                <td>{{$row->kontak}}</td>
                                <td>{{($row->status) == 1 ? 'Aktif' : 'Tidak Aktif'}}</td>
                                <td>
                                    <a href="{{route('pegawai.edit', $row->id)}}" class="btn btn-success btn-xs" type="button" data-original-title="Edit user ini" title="">Edit</a>
                                    <a href="#" class="btn btn-success btn-xs" type="button" data-original-title="Edit user ini" title="">Detail</a>
                                    <a href="{{route('pegawai.delete', $row->id)}}" class="btn btn-danger btn-xs" type="button" data-original-title="Hapus user ini" title="">Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Gender</th>
                                <th>Jabatan</th>
                                <th>Masa Kerja</th>
                                <th>Gaji</th>
                                <th>Alamat</th>
                                <th>No. Telp</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Base styles Ends-->
@endsection
