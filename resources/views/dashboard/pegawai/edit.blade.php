@extends('dashboard.layouts.template')

@section('container')

    <div class="card">
        <div class="card-header">
            <h5>{{ isset($pegawai) ? "Edit Pegawai" : "Tambah Pegawai" }}</h5>
        </div>
        <form class="form theme-form needs-validation" novalidate="" role="form" method="POST" action="{{ isset($pegawai) ? route('pegawai.update',$pegawai->id) : route('pegawai.create') }}">
            @csrf
            @isset($pegawai)
                @method('PUT')
            @endisset
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">NIK</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="number" placeholder="327312345678" value="{{ $pegawai->nik ?? '' }}" required="" name="nik">
                            <div class="invalid-feedback">NIK tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Nama Lengkap</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="text" placeholder="Zuchriyah Trimulyati" value="{{ $pegawai->namaPegawai ?? '' }}" required="" name="namaPegawai">
                            <div class="invalid-feedback">Nama tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Jenis Kelamin</label>
                            <select class="form-control digits" id="exampleFormControlSelect9" name="gender" required="">
                                @if(isset($pegawai))
                                <option {{ ($pegawai->gender === 'Perempuan') ? 'selected' : '' }}  value="Perempuan">Perempuan</option>
                                <option {{ ($pegawai->gender === 'Laki-Laki') ? 'selected' : '' }}  value="Laki-Laki">Laki-Laki</option>
                                @else
                                    <option value="Perempuan">Perempuan</option>
                                    <option value="Laki-Laki">Laki-Laki</option>
                                @endif

                            </select>
                            <div class="invalid-feedback">Nama tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Jabatan</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="text" placeholder="Admin" value="{{ $pegawai->jabatan ?? '' }}" required="" name="jabatan">
                            <div class="invalid-feedback">Jabatan tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="example-datetime-local-input">Mulai Kerja</label>
                                    <input class="form-control btn-pill"  type="date" value="{{isset($pegawai->mulaiKerja) ? date("Y-m-d", $pegawai->mulaiKerja) : ''}}" required="" name="mulaiKerja">
                                <div class="invalid-feedback">Waktu Mulai Kerja tidak boleh kosong</div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Gaji</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="number" value="{{$pegawai->gaji ?? ''}}" required="" name="gaji">
                            <div class="invalid-feedback">Gaji tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleFormControlTextarea4">Alamat</label>
                            <textarea class="form-control" id="exampleFormControlTextarea4" rows="3" required="" name="alamat">{{$pegawai->alamat ?? ''}}</textarea>
                            <div class="invalid-feedback">Alamat tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">No. Hp / Telepon</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="number" value="{{$pegawai->kontak ?? ''}}" required="" name="kontak">
                            <div class="invalid-feedback">Kontak tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Status</label>
                            <select class="form-control digits" id="exampleFormControlSelect9" name="status" required="">
                                @if(isset($pegawai))
                                <option {{ ($pegawai->status === 1)  ? 'selected' : '' }}  value="1">Aktif</option>
                                <option {{ ($pegawai->status === 2)  ? 'selected' : '' }}  value="2">Tidak Aktif</option>
                                @else
                                    <option value="1">Aktif</option>
                                    <option value="2">Tidak Aktif</option>
                                @endif
                            </select>
                            <div class="invalid-feedback">Status tidak boleh kosong</div>
                        </div>
                    </div>
                </div>

{{--                <div class="row">--}}
{{--                    <div class="col">--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="example-datetime-local-input">Tanggal Lahir</label>--}}
{{--                            <input class="form-control btn-pill" id="example-datetime-local-input" type="date" value="">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
                <a href="/pegawai" class="btn btn-light" >Cancel</a>
            </div>
        </form>
    </div>

@endsection
