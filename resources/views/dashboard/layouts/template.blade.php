
<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ asset('html/assets/images/favicon.png ') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('html/assets/images/favicon.png ') }}" type="image/x-icon">
    <title>SML - Jangan Ragu! Coba Dulu! Baru Menilai!</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/fontawesome.css ') }}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/icofont.css ') }}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/themify.css ') }}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/flag-icon.css ') }}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/feather-icon.css ') }}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/animate.css ') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/chartist.css ') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/date-picker.css ') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/select2.css') }}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/bootstrap.css ') }}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/style.css ') }}">
    <link id="color" rel="stylesheet" href="{{ asset('html/assets/css/color-1.css ') }}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('html/assets/css/responsive.css ') }}">
    <!-- latest jquery-->
<script src="{{ asset('html/assets/js/jquery-3.5.1.min.js ') }}"></script>
</head>
<body onload="startTime()">
<!-- tap on top starts-->
<div class="tap-top"><i data-feather="chevrons-up"></i></div>
<!-- tap on tap ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper compact-wrapper" id="pageWrapper">
    <!-- Page Header Start-->
    <div class="page-main-header">
        <div class="main-header-right row m-0">
            <form class="form-inline search-full" action="#" method="get">
                <div class="form-group w-100">
                    <div class="Typeahead Typeahead--twitterUsers">
                        <div class="u-posRelative">
                            <input class="demo-input Typeahead-input form-control-plaintext w-100" type="text" placeholder="Search Cuba .." name="q" title="" autofocus>
                            <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><i class="close-search" data-feather="x"></i>
                        </div>
                        <div class="Typeahead-menu"></div>
                    </div>
                </div>
            </form>
            <div class="main-header-left">
                <div class="logo-wrapper"><a href="index.html"><img class="img-fluid" src="{{ asset('html/assets/images/logo/logo.png ') }}" alt=""></a></div>
                <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="grid" id="sidebar-toggle"> </i></div>
            </div>
            <div class="left-menu-header col horizontal-wrapper pl-0">
                <ul class="horizontal-menu">

                </ul>
            </div>
            <div class="nav-right col-8 pull-right right-menu">
                <ul class="nav-menus">

                    <li>
                        <div class="mode"><i class="fa fa-moon-o " data-feather="moon"></i></div>
                    </li>

                    <li class="maximize"><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
                    <li class="profile-nav onhover-dropdown p-0">
                        <div class="media profile-media"><img class="b-r-10" src="{{ asset('html/assets/images/dashboard/profile.jpg ') }}" alt="">
                            <div class="media-body"><span>Riki</span>
                                <p class="mb-0 font-roboto">Admin <i class="middle fa fa-angle-down"></i></p>
                            </div>
                        </div>
                        <ul class="profile-dropdown onhover-show-div">
                            <li><i data-feather="user"></i><span>Account </span></li>
                            <li><i data-feather="mail"></i><span>Inbox</span></li>
                            <li><i data-feather="file-text"></i><span>Taskboard</span></li>
                            <li><i data-feather="settings"></i><span>Settings</span></li>
                            <form method="post" action="/logout">
                                @csrf
                            <button class="btn btn-sm" type="submit"> <i data-feather="log-in"> </i><span>Log out</span></button>
                            </form>
                        </ul>
                    </li>
                </ul>
            </div>
            <script id="result-template" type="text/x-handlebars-template">
                <div class="ProfileCard u-cf">
                    <div class="ProfileCard-avatar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay m-0"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg></div>
                    <div class="ProfileCard-details">
                        <div class="ProfileCard-realName">Riki</div>
                    </div>
                </div>
            </script>
            <script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
        </div>
    </div>
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper sidebar-icon">
        @include('dashboard.partials.sidebar')
        <div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-6">
                            <h3>
                                {{ $title }}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Dashboard</li>
                            </ol>
                        </div>
                        <div class="col-6">
                            <!-- Bookmark Start-->
                            <div class="bookmark pull-right">

                            </div>
                            <!-- Bookmark Ends-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid starts-->
            <div class="container-fluid">
                @yield('container')
            </div>
            <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 footer-copyright">
                        <p class="mb-0">Copyright 2020 © Cuba All rights reserved.</p>
                    </div>
                    <div class="col-md-6">
                        <p class="pull-right mb-0">Developed with  <i class="fa fa-heart font-secondary"></i></p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- Bootstrap js-->
<script src="{{ asset('html/assets/js/bootstrap/popper.min.js ') }}"></script>
<script src="{{ asset('html/assets/js/bootstrap/bootstrap.js ') }}"></script>
<!-- feather icon js-->
<script src="{{ asset('html/assets/js/icons/feather-icon/feather.min.js ') }}"></script>
<script src="{{ asset('html/assets/js/icons/feather-icon/feather-icon.js ') }}"></script>
<!-- Sidebar jquery-->
<script src="{{ asset('html/assets/js/sidebar-menu.js ') }}"></script>
<script src="{{ asset('html/assets/js/config.js ') }}"></script>
<!-- Plugins JS start-->
<script src="{{ asset('html/assets/js/chart/chartist/chartist.js ') }}"></script>
<script src="{{ asset('html/assets/js/chart/chartist/chartist-plugin-tooltip.js ') }}"></script>
<script src="{{ asset('html/assets/js/chart/knob/knob.min.js ') }}"></script>
<script src="{{ asset('html/assets/js/chart/knob/knob-chart.js ') }}"></script>
<script src="{{ asset('html/assets/js/chart/apex-chart/apex-chart.js ') }}"></script>
<script src="{{ asset('html/assets/js/chart/apex-chart/stock-prices.js ') }}"></script>
<script src="{{ asset('html/assets/js/notify/bootstrap-notify.min.js ') }}"></script>
<script src="{{ asset('html/assets/js/dashboard/default.js ') }}"></script>
<script src="{{ asset('html/assets/js/notify/index.js ') }}"></script>
<script src="{{ asset('html/assets/js/datepicker/date-picker/datepicker.js ') }}"></script>
<script src="{{ asset('html/assets/js/datepicker/date-picker/datepicker.en.js ') }}"></script>
<script src="{{ asset('html/assets/js/datepicker/date-picker/datepicker.custom.js ') }}"></script>
<script src="{{ asset('html/assets/js/typeahead/handlebars.js ') }}"></script>
<script src="{{ asset('html/assets/js/typeahead/typeahead.bundle.js ') }}"></script>
<script src="{{ asset('html/assets/js/typeahead/typeahead.custom.js ') }}"></script>
<script src="{{ asset('html/assets/js/typeahead-search/handlebars.js ') }}"></script>
<script src="{{ asset('html/assets/js/typeahead-search/typeahead-custom.js ') }}"></script>
<script src="{{ asset('html/assets/js/tooltip-init.js ') }}"></script>
<script src="{{ asset('html/assets/js/datatable/datatables/jquery.dataTables.min.js')  }}"></script>
<script src="{{ asset('html/assets/js/datatable/datatables/datatable.custom.js') }}"></script>
<script src="{{asset('html/assets/js/form-validation-custom.js')}}"></script>
<script src="{{asset('html/assets/js/datepicker/date-time-picker/moment.min.js')}}"></script>
<script src="{{asset('html/assets/js/datepicker/date-time-picker/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('html/assets/js/datepicker/date-time-picker/datetimepicker.custom.js')}}"></script>
<script src="{{ asset('html/assets/js/select2/select2.full.min.js') }} "></script>
<script src=" {{ asset('html/assets/js/select2/select2-custom.js') }}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{ asset('html/assets/js/script.js ') }}"></script>
<script src="{{ asset('html/assets/js/theme-customizer/customizer.js ') }}"></script>
<!-- login js-->
<!-- Plugin used-->

</body>
</html>
