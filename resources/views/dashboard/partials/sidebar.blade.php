<!-- Page Sidebar Start-->
<header class="main-nav">
    <div class="logo-wrapper"><a href="index.html"><img class="img-fluid for-light" src="{{ asset('html/assets/images/logo/logo.png ') }}" alt=""><img class="img-fluid for-dark" src="{{ asset('html/assets/images/logo/logo_dark.png ') }}" alt=""></a>
        <div class="back-btn"><i class="fa fa-angle-left"></i></div>
        <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="grid" id="sidebar-toggle"> </i></div>
    </div>
    <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="{{ asset('html/assets/images/logo/logo-icon.png ') }}" alt=""></a></div>
    <nav>
        <div class="main-navbar">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                    <li class="back-btn"><a href="index.html"><img class="img-fluid" src="{{ asset('html/assets/images/logo/logo-icon.png ') }}" alt=""></a>
                        <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-title">
                        <div>
                            <h6 class="lan-1">General</h6>
                            <p class="lan-2">Dashboards,data master & layout.</p>
                        </div>
                    </li>
                    <li class="dropdown"><a class="nav-link menu-title" href="/dashboard"><i data-feather="home"></i><span class="lan-3">Dashboard</span>
                           </a>
                    </li>

                    @if(session()->get("user") == "TZUCHRIYAH")
                    <li class="sidebar-title">
                        <div>
                            <h6>Data Master</h6>
                            <p>Ready to use Apps</p>
                        </div>
                    </li>
                    <li class="dropdown "><a class="nav-link menu-title active" href="/paket"><i data-feather="shopping-bag"></i><span>Paket Laundry</span></a>
                    </li>
                    <li class="dropdown "><a class="nav-link menu-title active" href="/barang"><i data-feather="layers"></i><span>Barang Laundry</span></a>
                    </li>
                    <li class="dropdown"><a class="nav-link menu-title" href="/pegawai"><i data-feather="users"></i><span>Pegawai</span></a>
                    </li>
                    <li class="dropdown"><a class="nav-link menu-title" href="/user"><i data-feather="user"></i><span>User</span></a>
                    </li>
                    @endif
                    <li class="sidebar-title">
                        <div>
                            <h6>Data Transaksi</h6>
                            <p>UI Components & Elements </p>
                        </div>
                    </li>
                    <li class="dropdown"><a class="nav-link menu-title" href="/transaksi"><i data-feather="box"></i><span>Transaksi</span></a>

                    </li>
                    <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>History</span></a>

                    </li>

                    <li class="dropdown"><a class="nav-link menu-title" href="/pelanggan"><i data-feather="users"></i><span>Pelanggan</span></a>

                    </li>
                    <li class="sidebar-title">
                        <div>
                            <h6>Utilities</h6>
                            <p>Ready to use froms & tables </p>
                        </div>
                    </li>
                    <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="file-text"></i><span>Laporan</span></a>
                        <ul class="nav-submenu menu-content">
                            <li><a class="submenu-title" href="#">Form Controls<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                                <ul class="nav-sub-childmenu submenu-content">
                                    <li><a href="form-validation.html">Form Validation</a></li>
                                    <li><a href="base-input.html">Base Inputs</a></li>
                                    <li><a href="radio-checkbox-control.html">Checkbox & Radio</a></li>
                                    <li><a href="input-group.html">Input Groups</a></li>
                                    <li><a href="megaoptions.html">Mega Options</a></li>
                                </ul>
                            </li>
                            <li><a class="submenu-title" href="#">Form Widgets<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                                <ul class="nav-sub-childmenu submenu-content">
                                    <li><a href="datepicker.html">Datepicker</a></li>
                                    <li><a href="time-picker.html">Timepicker</a></li>
                                    <li><a href="datetimepicker.html">Datetimepicker</a></li>
                                    <li><a href="daterangepicker.html">Daterangepicker</a></li>
                                    <li><a href="touchspin.html">Touchspin</a></li>
                                    <li><a href="select2.html">Select2</a></li>
                                    <li><a href="switch.html">Switch</a></li>
                                    <li><a href="typeahead.html">Typeahead</a></li>
                                    <li><a href="clipboard.html">Clipboard</a></li>
                                </ul>
                            </li>
                            <li><a class="submenu-title" href="#">Form layout<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                                <ul class="nav-sub-childmenu submenu-content">
                                    <li><a href="default-form.html">Default Forms</a></li>
                                    <li><a href="form-wizard.html">Form Wizard 1</a></li>
                                    <li><a href="form-wizard-two.html">Form Wizard 2</a></li>
                                    <li><a href="form-wizard-three.html">Form Wizard 3</a></li>
                                    <li><a href="form-wizard-four.html">Form Wizard 4</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="server"></i><span>Tables</span></a>
                        <ul class="nav-submenu menu-content">
                            <li><a class="submenu-title" href="#">Bootstrap Tables<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                                <ul class="nav-sub-childmenu submenu-content">
                                    <li><a href="bootstrap-basic-table.html">Basic Tables</a></li>
                                    <li><a href="bootstrap-sizing-table.html">Sizing Tables</a></li>
                                    <li><a href="bootstrap-border-table.html">Border Tables</a></li>
                                    <li><a href="bootstrap-styling-table.html">Styling Tables</a></li>
                                    <li><a href="table-components.html">Table components</a></li>
                                </ul>
                            </li>
                            <li><a class="submenu-title" href="#">Data Tables<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                                <ul class="nav-sub-childmenu submenu-content">
                                    <li><a href="datatable-basic-init.html">Basic Init</a></li>
                                    <li><a href="datatable-advance.html">Advance Init</a></li>
                                    <li><a href="datatable-styling.html">Styling</a></li>
                                    <li><a href="datatable-AJAX.html">AJAX</a></li>
                                    <li><a href="datatable-server-side.html">Server Side</a></li>
                                    <li><a href="datatable-plugin.html">Plug-in</a></li>
                                    <li><a href="datatable-API.html">API</a></li>
                                    <li><a href="datatable-data-source.html">Data Sources</a></li>
                                </ul>
                            </li>
                            <li><a class="submenu-title" href="#">Ex. Data Tables<span class="sub-arrow"><i class="fa fa-angle-right"></i></span></a>
                                <ul class="nav-sub-childmenu submenu-content">
                                    <li><a href="datatable-ext-autofill.html">Auto Fill</a></li>
                                    <li><a href="datatable-ext-basic-button.html">Basic Button</a></li>
                                    <li><a href="datatable-ext-col-reorder.html">Column Reorder</a></li>
                                    <li><a href="datatable-ext-fixed-header.html">Fixed Header</a></li>
                                    <li><a href="datatable-ext-html-5-data-export.html">HTML 5 Export</a></li>
                                    <li><a href="datatable-ext-key-table.html">Key Table</a></li>
                                    <li><a href="datatable-ext-responsive.html">Responsive</a></li>
                                    <li><a href="datatable-ext-row-reorder.html">Row Reorder</a></li>
                                    <li><a href="datatable-ext-scroller.html">Scroller</a></li>
                                </ul>
                            </li>
                            <li><a href="jsgrid-table.html">Js Grid Table</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </div>
    </nav>
</header>
<!-- Page Sidebar Ends-->
