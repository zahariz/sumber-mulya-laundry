@extends('dashboard.layouts.template')

@section('container')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="float-right"><i><b>#Invoice - {{ date('dmy', time()) . strtoupper(uniqid()) }}</b></i>
                    </h5>
                </div>
            </div>
        </div>
        <!-- EMI Starts-->

        <div class="col-lg-4 box-col-6">
            <div class="card">
                <div class="card-header">
                    <h5>Pelanggan</h5>
                </div>

                <form action="{{ route('transaksi.create') }}" class="form theme-form needs-validation" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <select id="idPelanggan" name="idPelanggan" onchange="updatePelanggan()"
                                        class="js-example-basic-single form-control">
                                        <option value="">-- Cari Member --</option>
                                        @foreach ($pelanggan as $row)
                                            <option value="{{ isset($row->id) ? $row->id : '' }}">
                                                {{ isset($row->namaPelanggan) ? $row->namaPelanggan : '' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <input id="to" class="form-control" type="text" placeholder="No. Kontak">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="3" placeholder="Alamat"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>


            </div>
        </div>
        <!-- EMI Ends-->

        <!-- Individual column searching (text inputs) Starts-->
        <div class="col-xl-4 col-xl-8 box-col-12">
            <div class="card">
                <div class="card-header">
                    <h5>Paket </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="theme-form mega-form">
                                <input class="form-control" type="hidden"
                                    placeholder="INV-{{ date('dmy', time()) . strtoupper(uniqid()) }}"
                                    value="{{ uniqid() }}" name="kdTransaksi" readonly>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <select id="paket" name="idPaket" onchange="paketTransaksi()"
                                                class="js-example-basic-single col-sm-12">
                                                <option value="">-- Cari Paket --</option>
                                                @foreach ($paket as $row)
                                                    <option value="{{ isset($row->id) ? $row->id : '' }}">
                                                        {{ isset($row->namaPaket) ? $row->namaPaket : '' }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input id="waktu" name="waktu" class="form-control" type="text" placeholder="Waktu"
                                                readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input id="beratPaket" name="beratPaket" onchange="hargaPaket" class="form-control"
                                                type="number" placeholder="Berat Paket">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input id="hargaPaket" name="hargaPaket" class="form-control" type="number"
                                                placeholder="Harga Paket">
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End indvidual column -->

        <!-- EMI Starts-->
        <div class="col-lg-8 box-col-6">
            <div class="card">
                <div class="card-header">
                    <h5>Detail Barang</h5>
                </div>
                <div class="card-body">
                    {{-- <form action="/transaksicart" method="POST" role="form" class="form theme-form needs-validation">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <select id="barang" name="barang" class="js-example-basic-single form-control">
                                        <option value="">-- Cari Barang --</option>
                                        @foreach ($barang as $row)
                                            <option value="{{ isset($row->id) ? $row->namaBarang : '' }}">
                                                {{ isset($row->namaBarang) ? $row->namaBarang : '' }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <input id="qty" name="qty" class="form-control" type="text" placeholder="Qty">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                    </form> --}}

                    <div class="table-responsive" >
                        <table class="table table-hover" id="TabelTransaksi">
                            <thead>
                                <tr>
                                    <th scope="col" style='width:35px;'>#</th>
                                    <th scope="col" style='width:210px;'>Nama Barang</th>
                                    <th scope="col" style='width:125px;'>Qty</th>
                                    <th scope="col" style='width:40px;'>Action</th>
                                </tr>
                            </thead>
                                <tbody>
                                    @foreach ($orderProducts as $index => $value )
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <select id='idBarang' name="idBarang[{{ $index }}]" class='js-example-basic-single form-control' >
                                                <option value=''> -- Cari Barang -- </option>
                                                @foreach($barang as $row)
                                                <option value ='{{ isset($row->id) ? $row->id : '' }}'  >{{ isset($row->namaBarang) ? $row->namaBarang : '' }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" id="jumlah_beli" class="form-control text-center" name="total[{{ $index }}]" onkeypress="return check_int(event)">
                                        </td>
                                        <td><a href="javascript:void(0)" class="text-success font-18" title="Add" id="addBtn"><i class="fa fa-plus"></i>Add</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <!-- EMI Ends-->

        <!-- EMI Starts-->
        <div class="col-lg-4 box-col-6">
            <div class="card">
                <div class="card-header">
                    <h5>Checkout</h5>
                </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <input class="form-control" type="number" id="grandTotal" name="grandTotal" placeholder="Grand Total" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <input id="bayar" class="form-control" name="bayar" type="number" placeholder="Bayar">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <input id="kembalian" class="form-control" name="kembalian" type="number" placeholder="Kembalian">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-primary btn-block" type="submit">Checkout</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- EMI Ends-->





    </div>
    <script type="text/javascript">





        function updatePelanggan() {
            let idPelanggan = $("#idPelanggan").val()
            let to = document.querySelector("#to");
            let alamat = document.querySelector("#alamat");
            if (idPelanggan !== '') {
                $.ajax({
                    url: "{{ url('') }}/transaksipelanggan/" + idPelanggan,
                    success: function(res) {
                        to.value = res.kontak;
                        alamat.value = res.alamat;
                    }
                });
            } else {
                to.value = null;
                alamat.value = null;
            }
        }

        function paketTransaksi() {
            let paket = $("#paket").val();
            let waktu = document.querySelector("#waktu");

            if (paket != '') {
                $.ajax({
                    url: "{{ url('') }}/transaksipaket/" + paket,
                    success: function(res) {
                        $('#beratPaket').each(function() {
                            $(this).keyup(function() {
                        $('#hargaPaket').val(res.hargaPaket * parseFloat($(this)
                                    .val() || 0));
                                    $('#grandTotal').val(res.hargaPaket * parseFloat($(this)
                                    .val() || 0));
                            });
                        });
                        var final = res.waktuPaket / 86400;
                        waktu.value = final;

                    }
                });
            } else {
                waktu.value = null
                $('#hargaPaket').val(null);
                $('#beratPaket').val(null);
            }
        }


        $('#bayar').each(function () {
            $(this).keyup(function() {
                 $('#kembalian').val( $(this).val() - $('#grandTotal').val());
            });

        });

        var rowIdx = 1;
        $("#addBtn").on("click", function ()
            {
                // Adding a row inside the tbody.
                $("#TabelTransaksi tbody").append(`
                <tr id="R${++rowIdx}">
                    <td class="row-index text-center"><p> ${rowIdx}</p></td>
                    <td><select id='idBarang' name="idBarang[]" class='js-example-basic-single form-control' >   <option value=''> -- Cari Barang -- </option> @foreach($barang as $row)<option value ='{{ isset($row->id) ? $row->id : '' }}' >{{ isset($row->namaBarang) ? $row->namaBarang : '' }}</option>  @endforeach  </select></td>
                    <td><input type="number" id="jumlah_beli" class="form-control text-center" name="total[]" onkeypress="return check_int(event)"></td>
                    <td><a href="javascript:void(0)" class="text-danger font-18 remove" title="Remove"><i class="fa fa-trash-o"></i>Remove</a></td>
                </tr>
                `);

            });

            $("#TabelTransaksi tbody").on("click", ".remove", function ()
            {
                // Getting all the rows next to the row
                // containing the clicked button
                var child = $(this).closest("tr").nextAll();
                // Iterating across all the rows
                // obtained to change the index
                child.each(function () {
                // Getting <tr> id.
                var id = $(this).attr("id");

                // Getting the <p> inside the .row-index class.
                var idx = $(this).children(".row-index").children("p");

                // Gets the row number from <tr> id.
                var dig = parseInt(id.substring(1));

                // Modifying row index.
                idx.html(`${dig - 1}`);

                // Modifying row id.
                $(this).attr("id", `R${dig - 1}`);
            });

                // Removing the current row.
                $(this).closest("tr").remove();

                // Decreasing total number of rows by 1.
                rowIdx--;
            });


        // function BarisBaru() {
        //     var Nomor = $('#TabelTransaksi tbody tr').length + 1;
        //     var Baris = "<tr>";
        //     Baris += "<td>" + Nomor + "</td>";
        //     Baris += "<td>";
        //     Baris += "<select id='idBarang' name='idBarang[]' class='js-example-basic-single form-control' > <option value=''> -- Cari Barang -- </option> @foreach($barang as $row) <option value ='{{ isset($row->id) ? $row->id : '' }}' name='idBarang[]' >{{ isset($row->namaBarang) ? $row->namaBarang : '' }}</option> @endforeach  </select>";
        //     Baris += "<div id='hasil_pencarian'></div>";
        //     Baris += "</td>";
        //     Baris +=
        //         '<td><input type="number" id="jumlah_beli" class="form-control text-center" name="total[]" onkeypress="return check_int(event)"></td>';
        //     Baris +=
        //         "<td><button class='btn btn-sm btn-danger removeButton' id='HapusBaris'  >Hapus</button></td>";
        //     Baris += "</tr>";

        //     $('#TabelTransaksi tbody').append(Baris);

        //     $('#TabelTransaksi tbody tr').each(function() {
        //         $(this).find('td:nth-child(2) input').focus();
        //     });
        // }

//         $(document).on('click', '#HapusBaris', function(e){
// 	e.preventDefault();
// 	$(this).parent().parent().remove();

// 	var Nomor = 1;
// 	$('#TabelTransaksi tbody tr').each(function(){
// 		$(this).find('td:nth-child(1)').html(Nomor);
// 		Nomor++;
// 	});
// });



//         $(document).on('click', '#HapusBaris', function(e){
// 	e.preventDefault();
// 	$(this).parent().parent().remove();

// 	var Nomor = 1;
// 	$('#TabelTransaksi tbody tr').each(function(){
// 		$(this).find('td:nth-child(1)').html(Nomor);
// 		Nomor++;
// 	});
// });


    </script>
@endsection
