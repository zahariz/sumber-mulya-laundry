@extends('dashboard.layouts.template')

@section('container')

    <div class="card">
        <div class="card-header">
            <h5>{{ isset($pelanggan) ? "Edit Pelanggan" : "Tambah Pelanggan" }}</h5>
        </div>
        <form class="form theme-form needs-validation" novalidate="" role="form" method="POST"
              action="{{ isset($pelanggan) ? route('pelanggan.update',$pelanggan->id) : route('pelanggan.create') }}">
            @csrf
            @isset($pelanggan)
                @method('PUT')
            @endisset
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Nama Pelanggan</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="text" name="namaPelanggan" value="{{ $pelanggan->namaPelanggan ?? '' }}" required="">
                            <div class="invalid-feedback">Nama pelanggan tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Kontak</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="number" name="kontak" value="{{ $pelanggan->kontak ?? '' }}" required="">
                            <div class="invalid-feedback">Kontak pelanggan tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleFormControlTextarea4">Alamat Pelanggan</label>
                            <textarea class="form-control" id="exampleFormControlTextarea4" rows="3" required="" name="alamat">{{$pelanggan->alamat ?? ''}}</textarea>
                            <div class="invalid-feedback">Alamat tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
                <a href="/pelanggan" class="btn btn-light" >Cancel</a>
            </div>
        </form>
    </div>

@endsection
