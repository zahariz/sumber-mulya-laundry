@extends('dashboard.layouts.template')

@section('container')
    <!-- Base styles-->
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>List Member Laundry </h5>
                    <a href="{{route('pelanggan.create')}}" class="btn btn-primary btn-sm float-right" type="button" data-original-title="Tambahkan List Paket Laundry" title="">Tambah Pelanggan</a>
                </div>
                <div class="card-body">
                    @if (session('msg'))
                        <div class="text-left alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="bell"></i>
                            <p>{{session('msg')}}</p>
                            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="display" id="example-style-1">
                            <thead>
                            <tr>
                                <th>Nama Pelanggan</th>
                                <th>Kontak</th>
                                <th>Alamat</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pelanggan as $row)
                                <tr>
                                    <td>{{$row->namaPelanggan}}</td>
                                    <td>{{$row->kontak}}</td>
                                    <td>{{$row->alamat}}</td>
                                    <td>
                                        <a href="{{route('pelanggan.edit', $row->id)}}" class="btn btn-success btn-xs" type="button" data-original-title="Edit paket ini" title="">Edit</a>
                                        <a href="{{route('pelanggan.delete', $row->id)}}" class="btn btn-danger btn-xs" type="button" data-original-title="Hapus paket ini" title="">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Kode Paket</th>
                                <th>Nama Paket</th>
                                <th>Harga</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Base styles Ends-->
@endsection
