@extends('dashboard.layouts.template')

@section('container')

    <div class="card">
        <div class="card-header">
            <h5>{{ isset($user) ? "Edit User" : "Tambah User" }}</h5>
        </div>
        <form class="form theme-form needs-validation" novalidate="" role="form" method="POST"
              action="{{ isset($user) ? route('user.update',$user->id) : route('user.create') }}">
            @csrf
            @isset($user)
                @method('PUT')
            @endisset
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleFormControlInput5">Username</label>
                            <input class="form-control btn-pill" id="exampleFormControlInput5" type="text" name="user" placeholder="TZUCHRIYAH" value="{{ $user->user ?? '' }}" required="">
                            <div class="invalid-feedback">Username tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Password</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="password" placeholder="" name="password" value="{{ $user->password ?? '' }}" required="">
                            <div class="invalid-feedback">Password tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="exampleInputText">Role</label>
                            <input class="form-control btn-pill" id="exampleInputText" type="text" placeholder="Admin" name="role" value="{{ $user->role ?? '' }}" required="">
                            <div class="invalid-feedback">Role tidak boleh kosong</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
                <a href="/user" class="btn btn-light" >Cancel</a>
            </div>
        </form>
    </div>

@endsection
