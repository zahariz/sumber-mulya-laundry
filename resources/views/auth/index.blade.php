@extends('dashboard.layouts.template')

@section('container')
    <!-- Base styles-->
    <div class="row second-chart-list third-news-update">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>User Admin </h5>
                    <a href="{{route('user.create')}}" class="btn btn-primary btn-sm float-right" type="button" data-original-title="Tambahkan List User Laundry" title="">Tambah User</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="example-style-1">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user as $row)
                                <tr>
                                    <td>{{$row->user}}</td>
                                    <td>{{$row->password}}</td>
                                    <td>{{$row->role}}</td>
                                    <td>
                                        <a href="{{route('user.edit', $row->id)}}" class="btn btn-success btn-xs" type="button" data-original-title="Edit paket ini" title="">Edit</a>
                                        <a href="{{route('user.delete', $row->id)}}" class="btn btn-danger btn-xs" type="button" data-original-title="Hapus paket ini" title="">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Base styles Ends-->
@endsection
