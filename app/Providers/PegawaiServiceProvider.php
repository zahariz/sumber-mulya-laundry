<?php

namespace App\Providers;

use App\Services\Impl\PegawaiServiceImpl;
use App\Services\PegawaiService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class PegawaiServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public array $singletons =[
      PegawaiService::class => PegawaiServiceImpl::class
    ];

    public function provides()
    {
        return [PegawaiService::class];
    }

    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
