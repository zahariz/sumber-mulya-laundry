<?php

namespace App\Providers;

use App\Services\Impl\LoginServiceImpl;
use App\Services\LoginService;
use Illuminate\Support\ServiceProvider;

class LoginServiceProvider extends ServiceProvider
{

    public array $singletons = [
      LoginService::class => LoginServiceImpl::class
    ];


    public function provides()
    {
        return [LoginService::class];
    }


    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
