<?php

namespace App\Providers;

use App\Services\BarangService;
use App\Services\Impl\BarangServiceImpl;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class BarangServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public array $singletons = [
        BarangService::class => BarangServiceImpl::class
    ];

    public function provides()
    {
        return [BarangService::class];
    }

    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
