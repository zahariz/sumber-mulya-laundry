<?php

namespace App\Providers;

use App\Services\Impl\PaketServiceImpl;
use App\Services\PaketService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class PaketServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public array $singletons = [
      PaketService::class => PaketServiceImpl::class
    ];

    public function provides()
    {
        return [PaketService::class];
    }

    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
