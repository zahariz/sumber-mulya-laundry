<?php

namespace App\Providers;

use App\Services\Impl\PelangganServiceImpl;
use App\Services\PelangganService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class PelangganServiceProvider extends ServiceProvider implements DeferrableProvider
{


    public array $singletons = [
      PelangganService::class => PelangganServiceImpl::class
    ];


    public function provides()
    {
        return [PelangganService::class];
    }

    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
