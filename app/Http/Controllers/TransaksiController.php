<?php

namespace App\Http\Controllers;

use App\Services\BarangService;
use App\Services\PaketService;
use App\Services\PelangganService;
use App\Services\TransaksiService;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{

    private PelangganService $pelangganService;
    private BarangService $barangService;
    private PaketService $paketService;
    private TransaksiService $transaksiService;
    public $orderProducts = [];

    /**
     * @param PelangganService $pelangganService
     */
    public function __construct(TransaksiService $transaksiService,PelangganService $pelangganService, PaketService $paketService, BarangService $barangService)
    {
        $this->barangService = $barangService;
        $this->pelangganService = $pelangganService;
        $this->paketService = $paketService;
        $this->transaksiService = $transaksiService;
    }

    public function showTransaksi(Request $request)
    {
        $pelanggan = $this->pelangganService->getAllPelanggan();
        $barang = $this->barangService->getAllBarang();
        $paket = $this->paketService->getAllPaket();
        $this->orderProducts = [
            ['barang_id' => '', 'quantity' => 1]
        ];
        return response()->view("dashboard.transaksi.index", [
            "title" => "Transaksi",
            "pelanggan" => $pelanggan,
            "paket" => $paket,
            "barang" => $barang,
            "orderProducts" => $this->orderProducts
        ]);
    }

    public function ajaxTransaksiPelanggan(Request $request, $id)
    {
        $value= $this->pelangganService->getPelangganById($id);
        if($request->ajax()){
          $json['kontak'] = isset($value->kontak) ? $value->kontak : "Tidak ada";
          $json['alamat'] = isset($value->alamat) ? $value->alamat : "Tidak ada";
            return response()->json($json);
        }
        $val = "Tidak ada";
        return response()->json($val);
    }

    public function ajaxTransaksiPaket(Request $request, $id)
    {
        $value= $this->paketService->getPaketById($id);
        if($request->ajax()){
            $json['waktuPaket'] = isset($value->waktuPaket) ? $value->waktuPaket : "Tidak ada";
            $json['hargaPaket'] = isset($value->harga) ? $value->harga : "Tidak ada";
            return response()->json($json);
        }
        $val = "Tidak ada";
        return response()->json($val);
    }

    public function saveTransaksi(Request $request, $id = null)
    {
        $collection = $request->except(['_token','_method']);

        if( ! is_null( $id ) )
        {
            $this->transaksiService->createOrUpdate($id, $collection);
        }
        else
        {
            $this->transaksiService->createOrUpdate($id = null, $collection);
        }

        return redirect()->route('paket.list')->with('msg', 'Data berhasil disimpan');
    }

}
