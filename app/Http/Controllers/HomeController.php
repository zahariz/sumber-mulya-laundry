<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    public function home(Request $request): Response|RedirectResponse
    {
        if ($request->session()->exists("user")){
            return response()->view("dashboard.analytic.index",[
                "title" => "Analytic Dashboard",
            ]);
        } else{
            return redirect("/login");
        }
    }
}
