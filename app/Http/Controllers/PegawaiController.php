<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OnlySuperAdminMiddleware;
use App\Services\PegawaiService;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    private PegawaiService $pegawaiService;

    /**
     * @param PegawaiService $pegawaiService
     */
    public function __construct(PegawaiService $pegawaiService)
    {
        $this->middleware(OnlySuperAdminMiddleware::class);
        $this->pegawaiService = $pegawaiService;
    }


    public function showPegawai()
    {
        $pegawai = $this->pegawaiService->getAllPegawai();
        return response()->view("dashboard.pegawai.index", [
            "title" => "Pegawai",
            "pegawai" => $pegawai
        ]);
    }

    public function createPegawai()
    {
        return response()->view("dashboard.pegawai.edit", [
            "title" => "Pegawai"
        ]);
    }

    public function getPegawai($id)
    {
        $pegawai = $this->pegawaiService->getPegawaiById($id);
        return response()->view("dashboard.pegawai.edit", [
            "title" => "Pegawai",
            "pegawai" => $pegawai
        ]);
    }

    public function getDetailPegawai($id)
    {
        $pegawai = $this->pegawaiService->getPegawaiById($id);
        return response()->view("dashboard.pegawai.detail", [
            "title" => "Pegawai",
            "pegawai" => $pegawai
        ]);
    }


    public function savePegawai(Request $request, $id = null)
    {
        $collection = $request->except(['_token','_method']);

        if( ! is_null( $id ) )
        {
            $this->pegawaiService->createOrUpdate($id, $collection);
        }
        else
        {
            $this->pegawaiService->createOrUpdate($id = null, $collection);
        }

        return redirect()->route('pegawai.list')->with('msg', 'Data berhasil disimpan');
    }

    public function deletePegawai($id)
    {
        $this->pegawaiService->deletePegawai($id);

        return redirect()->route('pegawai.list')->with('msg', 'Data berhasil dihapus');
    }




}
