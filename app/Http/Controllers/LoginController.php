<?php

namespace App\Http\Controllers;


use App\Services\LoginService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LoginController extends Controller
{

    private LoginService $loginService;

    /**
     * @param LoginService $loginService
     */
    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }


    public function login(): Response
    {
        return \response()
            ->view("auth.login",[
                "title" => "Login"
            ]);
    }

    public function doLogin(Request $request): Response|RedirectResponse
    {
        $user = $request->input('user');
        $password = $request->input('password');

        // validasi input
        if (empty($user) || empty($password)){
            return \response()->view("auth.login", [
                "title" => "Login",
                "error" => 'User atau password tidak boleh kosong'
            ]);
        }

        $verify = $this->loginService->login($user);

        // login sukses
        if ($verify != null && password_verify( $password,$verify->password)){
            $request->session()->put("user", $user);
            $request->session()->put("role", $verify->role);
            return redirect("/dashboard");
        } else {
            //  login gagal
            return \response()->view("auth.login", [
                "title" => "Login",
                "error" => "User atau password salah"
            ]);
        }
    }

    public function doLogout(Request $request): RedirectResponse
    {
        $request->session()->forget("user");
        $request->session()->forget("role");
        return redirect("/login");
    }


}
