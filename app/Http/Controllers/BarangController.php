<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OnlySuperAdminMiddleware;
use App\Models\Barang;
use App\Services\BarangService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BarangController extends Controller
{

    private BarangService $barangService;

    /**
     * @param BarangService $barangService
     */
    public function __construct(BarangService $barangService)
    {
        $this->middleware(OnlySuperAdminMiddleware::class);
        $this->barangService = $barangService;
    }

    public function showBarang()
    {
        $barang = $this->barangService->getAllBarang();
        return response()->view("dashboard.barang.index", [
            "title" => "Barang",
            "barang" => $barang
        ]);
    }

    public function createBarang()
    {
        return response()->view("dashboard.barang.edit", [
            "title" => "barang"
        ]);
    }

    public function getBarang($id)
    {
        $barang = $this->barangService->getBarangById($id);
        return response()->view("dashboard.barang.edit", [
            "title" => "Barang",
            "barang" => $barang
        ]);
    }

    public function saveBarang(Request $request, $id = null)
    {
        $collection = $request->except(['_token','_method']);

        if( ! is_null( $id ) )
        {
            $this->barangService->createOrUpdate($id, $collection);
        }
        else
        {
            $this->barangService->createOrUpdate($id = null, $collection);
        }

        return redirect()->route('barang.list')->with('msg', 'Data berhasil disimpan');
    }

    public function deleteBarang($id)
    {
        $this->barangService->deleteBarang($id);

        return redirect()->route('barang.list')->with('msg', 'Data berhasil dihapus');
    }
}
