<?php

namespace App\Http\Controllers;

use App\Services\PelangganService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PelangganController extends Controller
{

    private PelangganService $pelangganService;

    public function __construct(PelangganService $pelangganService)
    {
        $this->pelangganService = $pelangganService;
    }

    public function showPelanggan()
    {
        $pelanggan = $this->pelangganService->getAllPelanggan();
        return response()->view("dashboard.pelanggan.index", [
           "title" => "Pelanggan",
            "pelanggan" => $pelanggan
        ]);
    }

    public function createPelanggan()
    {
        return response()->view("dashboard.pelanggan.edit", [
            "title" => "Tambah Pelanggan"
        ]);
    }

    public function getPelangganById($id)
    {

        $pelanggan = $this->pelangganService->getPelangganById($id);
        return response()->view("dashboard.pelanggan.edit", [
           "title" => "Edit Pelanggan",
           "pelanggan" => $pelanggan
        ]);
    }

    public function createOrUpdate(Request $request, $id = null)
    {
        $collection = $request->except(['_token','_method']);

        if( ! is_null( $id ) )
        {
            $this->pelangganService->createOrUpdate($id, $collection);
        }
        else
        {
            $this->pelangganService->createOrUpdate($id = null, $collection);
        }

        return redirect()->route('pelanggan.list')->with('msg', 'Data berhasil disimpan');
    }

    public function deletePelanggan($id)
    {
        $pelanggan = $this->pelangganService->deletePelanggan($id);
        return redirect()->route("pelanggan.list")->with('msg', 'Data berhasil dihapus');
    }


}
