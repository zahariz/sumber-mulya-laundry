<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OnlySuperAdminMiddleware;
use App\Services\PaketService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PaketController extends Controller
{
    private PaketService $paketService;

    /**
     * @param PaketService $paketService
     */
    public function __construct(PaketService $paketService)
    {
        $this->middleware(OnlySuperAdminMiddleware::class);
        $this->paketService = $paketService;
    }

    public function showPaket()
    {
        $paket = $this->paketService->getAllPaket();
        return response()->view("dashboard.paket.index", [
            "title" => "Paket",
            "paket" => $paket
        ]);
    }

    public function createPaket()
    {
        return response()->view("dashboard.paket.edit", [
            "title" => "Paket"
        ]);
    }

    public function getPaket($id)
    {
        $paket = $this->paketService->getPaketById($id);
        return response()->view("dashboard.paket.edit", [
            "title" => "Paket",
            "paket" => $paket
        ]);
    }

    public function savePaket(Request $request, $id = null)
    {
        $collection = $request->except(['_token','_method']);

        if( ! is_null( $id ) )
        {
            $this->paketService->createOrUpdate($id, $collection);
        }
        else
        {
            $this->paketService->createOrUpdate($id = null, $collection);
        }

        return redirect()->route('paket.list')->with('msg', 'Data berhasil disimpan');
    }

    public function deletePaket($id)
    {
        $this->paketService->deletePaket($id);

        return redirect()->route('paket.list')->with('msg', 'Data berhasil dihapus');
    }


}
