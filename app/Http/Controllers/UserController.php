<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OnlySuperAdminMiddleware;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->middleware(OnlySuperAdminMiddleware::class);
        $this->userService = $userService;
    }

    public function showUser(): Response
    {
        $user = $this->userService->getAllUser();
        return response()->view("auth.index",[
           "title" => "User",
           "user" => $user
        ]);
    }

    public function createUser()
    {
        return response()->view("auth.edit", [
            "title" => "User"
        ]);
    }

    public function getUser($id)
    {
        $user = $this->userService->getUserById($id);
        return response()->view("auth.edit", [
            "title" => "User",
            "user" => $user
        ]);
    }

    public function saveUser(Request $request, $id = null)
    {
        $collection = $request->except(['_token','_method']);

        if( ! is_null( $id ) )
        {
            $this->userService->createOrUpdate($id, $collection);
        }
        else
        {
            $this->userService->createOrUpdate($id = null, $collection);
        }

        return redirect()->route('user.list');
    }

    public function deleteUser($id)
    {
        $this->userService->deleteUser($id);

        return redirect()->route('user.list');
    }
}
