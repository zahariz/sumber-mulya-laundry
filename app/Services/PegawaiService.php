<?php

namespace App\Services;

interface PegawaiService
{

    public function getAllPegawai();

    public function getPegawaiById($id);

    public function createOrUpdate( $id = null, $collection = [] );

    public function deletePegawai($id);

}
