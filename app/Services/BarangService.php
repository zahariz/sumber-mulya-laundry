<?php

namespace App\Services;

interface BarangService
{

    public function getAllBarang();

    public function getBarangById($id);

    public function createOrUpdate( $id = null, $collection = [] );

    public function deleteBarang($id);
}
