<?php

namespace App\Services\Impl;

use App\Models\Barang;
use App\Services\BarangService;


class BarangServiceImpl implements BarangService
{


    protected $barang = null;

    public function getAllBarang()
    {
        return Barang::all();
    }

    public function getBarangById($id)
    {
        return Barang::find($id);
    }

    public function createOrUpdate($id = null, $collection = [])
    {
        if(is_null($id)) {
            $barang = new Barang();
            $barang->kdBarang = $collection['kdBarang'];
            $barang->namaBarang = $collection['namaBarang'];
            return $barang->save();
        }
        $barang = Barang::find($id);
        $barang->kdBarang = $collection['kdBarang'];
        $barang->namaBarang = $collection['namaBarang'];
        return $barang->save();
    }

    public function deleteBarang($id)
    {
        return Barang::find($id)->delete();
    }

}
