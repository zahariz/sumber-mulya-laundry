<?php

namespace App\Services\Impl;

use App\Models\Paket;
use App\Services\PaketService;

class PaketServiceImpl implements PaketService
{
    protected $paket = null;

    public function createOrUpdate($id = null, $collection = [])
    {
        if(is_null($id)) {
            $paket = new Paket();
            $paket->kdPaket = $collection['kdPaket'];
            $paket->namaPaket = $collection['namaPaket'];
            $paket->harga = $collection['harga'];
            $paket->waktuPaket = $collection['waktuPaket'] * 86400;
            return $paket->save();
        }
        $paket = Paket::find($id);
        $paket->kdPaket = $collection['kdPaket'];
        $paket->harga = $collection['harga'];
        $paket->namaPaket = $collection['namaPaket'];
        $paket->waktuPaket = $collection['waktuPaket'] * 86400;
        return $paket->save();
    }

    public function deletePaket($id)
    {
        return Paket::find($id)->delete();
    }

    public function getAllPaket()
    {
       return Paket::all();
    }

    public function getPaketById($id)
    {
        return Paket::find($id);
    }

}
