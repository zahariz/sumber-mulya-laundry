<?php

namespace App\Services\Impl;

use App\Models\Pegawai;
use App\Services\PegawaiService;

class PegawaiServiceImpl implements PegawaiService
{

    protected $pegawai = null;

    public function getAllPegawai()
    {
        return Pegawai::all();
    }

    public function getPegawaiById($id)
    {
        return Pegawai::find($id);
    }

    public function createOrUpdate($id = null, $collection = [])
    {
        if(is_null($id)) {
            $pegawai = new Pegawai();
            $pegawai->nik = $collection['nik'];
            $pegawai->namaPegawai = $collection['namaPegawai'];
            $pegawai->gender = $collection['gender'];
            $pegawai->jabatan = $collection['jabatan'];
            $pegawai->mulaiKerja = strtotime($collection['mulaiKerja']);
            $pegawai->gaji = $collection['gaji'];
            $pegawai->alamat = $collection['alamat'];
            $pegawai->kontak = $collection['kontak'];
            $pegawai->status = $collection['status'];
            return $pegawai->save();
        }
        $pegawai = Pegawai::find($id);
        $pegawai->nik = $collection['nik'];
        $pegawai->namaPegawai = $collection['namaPegawai'];
        $pegawai->gender = $collection['gender'];
        $pegawai->jabatan = $collection['jabatan'];
        $pegawai->mulaiKerja = strtotime($collection['mulaiKerja']);
        $pegawai->gaji = $collection['gaji'];
        $pegawai->alamat = $collection['alamat'];
        $pegawai->kontak = $collection['kontak'];
        $pegawai->status = $collection['status'];
        return $pegawai->save();
    }

    public function deletePegawai($id)
    {
        return Pegawai::find($id)->delete();
    }

}
