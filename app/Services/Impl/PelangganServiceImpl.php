<?php

namespace App\Services\Impl;

use App\Models\Pelanggan;
use App\Services\PelangganService;

class PelangganServiceImpl implements PelangganService
{

    protected $pelanggan = null;

    public function getAllPelanggan()
    {
        return Pelanggan::all();
    }

    public function getPelangganById($id)
    {
        return Pelanggan::find($id);
    }

    public function createOrUpdate($id = null, $collection = [])
    {
       if (is_null($id))
       {
           $pelanggan = new Pelanggan();
           $pelanggan->namaPelanggan = $collection['namaPelanggan'];
           $pelanggan->kontak = $collection['kontak'];
           $pelanggan->alamat = $collection['alamat'];
           return $pelanggan->save();
       }
       $pelanggan = Pelanggan::find($id);
       $pelanggan->namaPelanggan = $collection['namaPelanggan'];
       $pelanggan->kontak = $collection['kontak'];
       $pelanggan->alamat = $collection['alamat'];
       return $pelanggan->save();
    }

    public function deletePelanggan($id)
    {
        return Pelanggan::find($id)->delete();
    }

}
