<?php

namespace App\Services\Impl;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserServiceImpl Implements UserService
{

    protected $user = null;

    public function createOrUpdate($id = null, $collection = [])
    {
       if (is_null($id))
       {
           $user = new User();
           $user->user = $collection['user'];
           $user->password = password_hash($collection['password'], PASSWORD_DEFAULT);
           $user->role = $collection['role'];
           return $user->save();
       }
        $user = User::find($id);
        $user->user = $collection['user'];
        $user->password = password_hash($collection['password'], PASSWORD_DEFAULT);
        $user->role = $collection['role'];
        return $user->save();
    }

    public function deleteUser($id)
    {
        return User::find($id)->delete();
    }

    public function getAllUser()
    {
        return User::all();
    }

    public function getUserById($id)
    {
        return User::find($id);
    }

}
