<?php

namespace App\Services\Impl;

use App\Models\User;
use App\Services\LoginService;
use Illuminate\Support\Facades\DB;

class LoginServiceImpl implements LoginService
{

    public function login(string $user)
    {
        $username = DB::table('users')
            ->where('user', '=', $user)
            ->first();

        if (isset($username)){
            return $username;
        } else {
            return null;
        }
    }

}
