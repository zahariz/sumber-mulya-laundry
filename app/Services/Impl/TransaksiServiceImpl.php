<?php

namespace App\Services\Impl;

use App\Models\Transaksi;
use App\Models\detailTransaksi;
use App\Services\TransaksiService;
use Illuminate\Support\Facades\Session;

class TransaksiServiceImpl implements TransaksiService
{

    protected $transaksi = null;
    protected $detailTransaksi = null;

    public function getAllTransaksi(){
        return Transaksi::all();
    }

    public function getTransaksiById($id){

    }


    public function createOrUpdate( $id = null, $collection = [] ){

        if(\is_null($id)){
            $transaksi = new Transaksi();
            $transaksi->kdTransaksi = $collection['kdTransaksi'];
            $transaksi->tglTransaksi = time();
            $transaksi->grandTotal = $collection['hargaPaket'];
            $transaksi->bayar = $collection['bayar'];
            $transaksi->keterangan = 'asd';
            $transaksi->pelanggan_id = $collection['idPelanggan'][0];
            $transaksi->user_id = 3;
           //$transaksi->save();
            foreach ($collection as $index => $value) {
                \dd($index);
                $detailTransaksi = new detailTransaksi();
                $detailTransaksi->kdTransaksi = $value['kdTransaksi'];
                $detailTransaksi->idPaket = $value['idPaket'];
                $detailTransaksi->idBarang = $value['idBarang'];
                $detailTransaksi->beratPaket = $value['beratPaket'];
                $detailTransaksi->waktu = $value['waktu'];
                $detailTransaksi->total = $value['total'];
                return $detailTransaksi->save();
            }

        }
        $transaksi = Transaksi::find($id);
        $transaksi->kdTransaksi = $collection['kdTransaksi'];
        $transaksi->tglTransaksi = time();
        $transaksi->grandTotal = $collection['grandTotal'];
        $transaksi->bayar = $collection['bayar'];
        $transaksi->keterangan = 'asd';
        $transaksi->pelanggan_id = $collection['idPelanggan'];

    }

    public function deleteTransaksi($id){

    }

}
