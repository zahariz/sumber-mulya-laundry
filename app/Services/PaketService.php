<?php

namespace App\Services;

interface PaketService
{

    public function getAllPaket();

    public function getPaketById($id);

    public function createOrUpdate( $id = null, $collection = [] );

    public function deletePaket($id);

}
