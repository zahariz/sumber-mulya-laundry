<?php

namespace App\Services;

interface UserService
{


    public function getAllUser();

    public function getUserById($id);

    public function createOrUpdate( $id = null, $collection = [] );

    public function deleteUser($id);

}
