<?php

namespace App\Services;

interface PelangganService
{

    public function getAllPelanggan();

    public function getPelangganById($id);

    public function createOrUpdate( $id = null, $collection = [] );

    public function deletePelanggan($id);

}
