<?php

namespace App\Services;

interface LoginService
{

    function login(string $user);

}
