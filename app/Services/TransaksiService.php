<?php

namespace App\Services;

interface TransaksiService
{

    public function getAllTransaksi();

    public function getTransaksiById($id);

    public function createOrUpdate( $id = null, $collection = [] );

    public function deleteTransaksi($id);

}
