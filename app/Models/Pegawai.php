<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    use HasFactory;

    protected $fillable = [
        'nik',
        'namaBarang',
        'namaPegawai',
        'gender',
        'jabatan',
        'mulaiKerja',
        'gaji',
        'alamat',
        'kontak',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
